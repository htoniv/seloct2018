package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadpage extends ProjectMethods{
	public static String FirstName;
	public CreateLeadpage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how =How.ID, using ="createLeadForm_companyName") WebElement elecompName;
	@FindBy(how =How.ID, using="createLeadForm_firstName") WebElement elefirstName;
	@FindBy(how =How.ID, using="createLeadForm_lastName") WebElement elelastName;
	@FindBy(how =How.CLASS_NAME, using="smallSubmit") WebElement elecreatelead;


	//Enter Company Name
	public CreateLeadpage enterCompanyName(String CName)

	{
		type(elecompName,CName);
		return this;
	}
	//Enter First Name
	public CreateLeadpage enterFirstName(String  FName)
	{
		type(elefirstName,FName);
		FirstName=FName;
		return this;
	}
	//Enter Last Name
	public CreateLeadpage enterLastName(String LName)
	{
		type(elelastName,LName);
		return this;
	}
	public MyLeadspage clickLogin()
	{
		click(elecreatelead);
		return new MyLeadspage();
	}
}






