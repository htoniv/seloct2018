package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LogoutPage extends ProjectMethods {
	
	public LogoutPage() {
		PageFactory.initElements(driver, this);
	}
	//page factory @FindBy-> similar to your locateElement
	/*@FindBy(how =How.ID, using ="username") WebElement eleUserName;
	@FindBy(how =How.ID, using="password") WebElement elePassword;*/
	@FindBy(how =How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	@FindBy(how =How.LINK_TEXT, using="CRM/SFA") WebElement elecrm;
	
	
	//login
	
		public LoginPage clickLogout() {
			click(eleLogout);
			LoginPage Lp= new LoginPage();
			
			return Lp;
						
		}
		public MyHomepage clickCrmSfa()
		{
			click(elecrm);
			MyHomepage mh= new MyHomepage();
			return mh;
		}
		
}
