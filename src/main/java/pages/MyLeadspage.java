package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadspage extends ProjectMethods{
public MyLeadspage() {
	PageFactory.initElements(driver, this);
}
@FindBy(how =How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;


public CreateLeadpage createLead() {
	click(eleCreateLead);
	CreateLeadpage cl= new CreateLeadpage();
	return cl; 
}


/*public MergeLeads clickMergeLead()
{
	click(eleMLeads);
	return new MergeLeads();
}
*/

}
