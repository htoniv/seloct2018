package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	//page factory @FindBy-> similar to your locateElement
	@FindBy(how =How.ID, using ="username") WebElement eleUserName;
	@FindBy(how =How.ID, using="password") WebElement elePassword;
	@FindBy(how =How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;
	
	//Enter user name
	public LoginPage enterUserName(String uName) {
		//WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);
		return	this;
				}
	//Enter Password
	public LoginPage enterPassword(String uPassword){
		type(elePassword, uPassword);
		return this;
	}
	//login
	
	public LogoutPage clickLogin() {
		click(eleLogin);
		//LogoutPage hp = new LogoutPage();
		//return hp;
		return new LogoutPage();
		
	}
	
	
	

}
