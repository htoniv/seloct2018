package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHomepage extends ProjectMethods {
	public MyHomepage() {
		PageFactory.initElements(driver, this);
	}
		
	
	//page factory @FindBy-> similar to your locateElement
	@FindBy(how =How.LINK_TEXT, using="Leads") WebElement eleLeads;
	//Enter user name
	
	public MyLeadspage clickLeads() {
		click(eleLeads);
		MyLeadspage ml= new MyLeadspage();
		return ml; 
	}
	
	
}
