package testCases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;



public class TC001_LoginLogout extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="Login Logout";
		
		testDesc="Login into Leaftap";
		author="vinoth";
		category="smoke";
		dataSheetName="TC001";
		
	}

	@Test(dataProvider="fetchData")
	public void loginLogout(String uname, String password) {
		new LoginPage()
		.enterUserName(uname)
		.enterPassword(password)
		.clickLogin()
		.clickLogout();
		
	}
}
