package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="Login Logout";
		
		testDesc="Login into Leaftap";
		author="vinoth";
		category="smoke";
		dataSheetName="createLead";
		
	}

	@Test(dataProvider="fetchData")
	public void loginLogout(String CName, String FName, String LName) {
		new LoginPage()
		.enterUserName("DemoSalesManager")
		.enterPassword("crmsfa")
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.createLead()
		.enterCompanyName(CName)
		.enterFirstName(FName)
		.enterLastName(LName)
		.clickLogin();
		
	
	
	}
}
