package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import excel.ReadExcel;

public class ProjectMethods extends SeMethods{
	public String dataSheetName;
	@BeforeTest(groups = {"any"})
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}
	@BeforeClass(groups = {"any"})
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}
	@Parameters({"url","username","password"})
	@BeforeMethod(groups = {"any"})
	public void login() {
		beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crm = locateElement("linkText", "CRM/SFA");
		click(crm);		*/
	}
	
	@AfterMethod(groups = {"any"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterClass(groups = {"any"})
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest(groups = {"any"})
	public void afterTest() {
		System.out.println("@AfterTest");
	}
	@AfterSuite(groups = {"any"})
	public void afterSuite() {
		endResult();
	}
	
	@BeforeSuite(groups = {"any"})
	public void beforeSuite() {
		startResult();
	}
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException
	{
	 return ReadExcel.readexcel(dataSheetName);
	}
}






